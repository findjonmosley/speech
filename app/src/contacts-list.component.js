import './contacts-list.style.scss';

export default {
  template:
`
<div class="contacts-list">
  <h4>Contacts</h4>
  <p ng-if="!contacts.length">You have no contacts.</p>
  <ul ng-if="contacts.length">
    <li class="contact-item" ng-repeat="contact in contacts">
      <button ng-click="onClickedDelete(contact._id)">Delete Contact</button>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Photo</th>
            <th>Company</th>
            <th>Job Title</th>
            <th>Phone</th>
            <th>Birthday</th>
            <th>Address</th>
            <th>Notes</th>
          </tr>
        </thead>
        <tr>
          <td ng-bind="contact._id"></td>
          <td ng-bind="contact.fname"></td>
          <td ng-bind="contact.lname"></td>
          <td ng-bind="contact.email"></td>
          <td ng-bind="contact.photo"></td>
          <td ng-bind="contact.company"></td>
          <td ng-bind="contact.jobtitle"></td>
          <td ng-bind="contact.phone"></td>
          <td ng-bind="contact.birthday"></td>
          <td ng-bind="contact.address"></td>
          <td ng-bind="contact.notes"></td>
        </tr>
      </table>

    </li>
  <ul>

</div>
`,
  controller: function($scope, ContactsService) {

    this.$onInit = () => {
      this.model;
    };
    $scope.contacts = [];

    this.$onChanges = (changes) => {
      if(changes.model.currentValue.model) {
        $scope.contacts.push(changes.model.currentValue.model);
      }else{
        ContactsService.retrieve().then((response) => {
          console.log('normal retrieve', response);
          $scope.contacts = response.data;
        });
      }
    };

    $scope.onClickedDelete = (id) => {
      ContactsService.delete(id).then((response) => {

        ContactsService.retrieve().then((response) => {
          $scope.contacts = response.data;
        });

      });
    };
  },
  bindings: {
    model: '<',
    onUpdate: '&'
  },
};
