import './contact-form.style.scss';

export default {
  template:
`
<form name="contactsForm" class="contact-form" novalidate>

  <span class="contact-form__group">
    <label class="contact-form__label" for="fname">First name *</label>
    <input class="contact-form__input" type="text" name="fname" id="fname" ng-model="$ctrl.model.fname" ng-pattern="{{'alphaNumeric'}}" required>

    <p class="contact-form__error" ng-show="contactsForm.fname.$touched && contactsForm.fname.$error.required">First name cant be empty</p>
    <p class="contact-form__error" ng-show="contactsForm.fname.$touched && contactsForm.fname.$error.pattern">The first name field can only be alpha numeric</p>
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="lname">Last name *</label>
    <input class="contact-form__input" type="text" name="lname" id="lname" ng-model="$ctrl.model.lname" ng-pattern="{{'alphaNumeric'}}" required>

    <p class="contact-form__error" ng-show="contactsForm.lname.$touched && contactsForm.lname.$error.required">Last name cant be empty</p>
    <p class="contact-form__error" ng-show="contactsForm.lname.$touched && contactsForm.lname.$error.pattern">The last name field can only be alpha numeric</p>
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="email">Email address *</label>
    <input class="contact-form__input" type="text" name="email" id="email" ng-model="$ctrl.model.email" ng-pattern="{{'email'}}" required>

    <p class="contact-form__error" ng-show="contactsForm.email.$touched && contactsForm.email.$error.required">Email cant be empty</p>
    <p class="contact-form__error" ng-show="contactsForm.email.$touched && contactsForm.email.$error.pattern">Please enter a valid email address</p>
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="photo">Photo</label>
    <input class="contact-form__input" type="text" name="photo" id="photo" ng-model="$ctrl.model.photo">
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="company">Company name</label>
    <input class="contact-form__input" type="text" name="company" id="company" ng-model="$ctrl.model.company">
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="jobtitle">Job title</label>
    <input class="contact-form__input" type="text" name="jobtitle" id="jobtitle" ng-model="$ctrl.model.jobtitle">
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="phone">Phone number</label>
    <input class="contact-form__input" type="text" name="phone" id="phone" ng-model="$ctrl.model.phone">
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="birthday">Birthday</label>
    <input class="contact-form__input" type="text" name="birthday" id="birthday" ng-model="$ctrl.model.birthday">
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="address">Address</label>
    <input class="contact-form__input" type="text" name="address" id="address" ng-model="$ctrl.model.address">
  </span>

  <span class="contact-form__group">
    <label class="contact-form__label" for="notes">Notes</label>
    <input class="contact-form__input" type="text" name="notes" id="notes" ng-model="$ctrl.model.notes">
  </span>

  <button class="contact-form__button" type="submit" ng-click="onClickedSubmit($ctrl.model, contactsForm)" ng-disabled="contactsForm.$invalid">
    Add
  </button>
</form>
`,
  controller: function($scope, $window, ContactsService, ValidationService) {

    this.$onInit = () => {
      this.model;
      this.onUpdate;
    };

    $scope.alphaNumeric = ValidationService.alphaNumeric();
    $scope.email        = ValidationService.email();

    $scope.onClickedSubmit = (model, form) => {

      if(form.$valid) {
        ContactsService.create(model).then((response) => {
          this.onUpdate({
            $event: {
              model: response[0]
            }
          });
        });

        // form reset
        $scope.contactsForm.$setPristine();
        $scope.contactsForm.$setUntouched();
        this.model = {};

        //ContactsService.update([{'fname': 'ELON'}, {'fname': 'BILL', 'lname': 'GATES'}]);
      }
    };

  },
  bindings: {
    model: '<',
    onUpdate: '&'
  }
};
