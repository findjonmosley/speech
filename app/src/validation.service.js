export default () => {
  return {
    alphaNumeric: () => {
        return /^[A-Za-z0-9]+$/;
    },
    email: () => {
      return /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
    }
  };
};
