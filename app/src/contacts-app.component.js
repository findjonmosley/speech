export default {
  template:
`
<contact-form model="$ctrl.model" on-update="$ctrl.update($event)"></contact-form>
<contacts-list model="$ctrl.model" on-update="$ctrl.update($event)"></contacts-list>
`,
  controller: function($scope) {
    this.model = {};

    this.update = (event) => {
      console.log('parent update', event);
      this.model = event;
    }
  }
};
